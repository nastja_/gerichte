from django.shortcuts import render, get_object_or_404, get_list_or_404
from . import models
from django.urls import reverse
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseNotFound
from django.utils import timezone
from datetime import datetime, timedelta, date
from django.db.models import Q
from django.http import JsonResponse


def index(request):
    return render(request, "app/index.html")


def auswahl(request):
    gerichte = models.Gericht.objects.filter(zum_auswahl=True)
    print(gerichte)
    return JsonResponse({ "gerichte": gerichte })

