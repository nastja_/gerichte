$.ajaxSetup({
    beforeSend: function(xhr) {
        xhr.setRequestHeader("X-CSRFToken", $("[name=csrfmiddlewaretoken]").val());
    }
});

new Vue(
  {
    el: "#app",
    delimiters: ['[[', ']]'],
    data: {
      max: 2,
      wahl: [],
      gerichte:[]
    },
    methods: {
      gerichteAuswahl() {
        console.log("nachgefragt")
        $.getJSON("/app/auswahl/", (data) => {
            this.gerichte = data.gerichte;
            console.log(data);
        });
        console.log(this.gerichte);
        },
      },
      "mounted": function() {
        this.gerichteAuswahl();
      },
    },
);